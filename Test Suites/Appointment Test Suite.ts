<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Appointment Test Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>3</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>true</rerunImmediately>
   <testSuiteGuid>cc7ef60f-bbad-4216-95b4-905e1f0e7576</testSuiteGuid>
   <testCaseLink>
      <guid>e693ffe5-5ea9-4e03-a067-98113d9662ec</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Fail</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>e0a28fae-0b39-4a71-a75e-92513510f8a5</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6eea53c1-31d2-4177-b9ef-b7ba86b87b7f</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>6557a7ea-ab6b-4569-ba1c-4908f544057d</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e9d99106-a548-45a8-9021-9f668cba5307</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Change Password Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>32c486c4-9c05-4ddd-ace1-0d6249f2c0cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Fail</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d816e28b-1755-49a8-b4b6-848485aeadaf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login Success</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>636cffcb-f898-48e9-9a9a-4a2320ac3d60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Logout</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
